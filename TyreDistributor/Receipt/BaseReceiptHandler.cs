﻿namespace TyreDistributor
{

    // todo we might be able to refact this further to allow us to simplify the generation of the 2 outputs but restricted due to time. 
    
    public abstract class BaseReceiptHandler : IReceiptHandler
    {
        public abstract string GetReceipt(Order order);

        protected static double CalculateAmounts(Line line)
        {
            var thisAmount = 0d;
            switch (line.Tyre.Model)
            {
                case Tyre.Suv:
                    if (line.Quantity >= 20)
                        thisAmount += line.Quantity * line.Tyre.Price * .9d;
                    else
                        thisAmount += line.Quantity * line.Tyre.Price;
                    break;
                case Tyre.Mini:
                    if (line.Quantity >= 10)
                        thisAmount += line.Quantity * line.Tyre.Price * .8d;
                    else
                        thisAmount += line.Quantity * line.Tyre.Price;
                    break;
                case Tyre.Estate:
                    if (line.Quantity >= 5)
                        thisAmount += line.Quantity * line.Tyre.Price * .8d;
                    else
                        thisAmount += line.Quantity * line.Tyre.Price;
                    break;
            }

            return thisAmount;
        }
    }
}