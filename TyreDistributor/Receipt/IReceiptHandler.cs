﻿namespace TyreDistributor
{
    public interface IReceiptHandler
    {
        string GetReceipt(Order order);
     
    }
}