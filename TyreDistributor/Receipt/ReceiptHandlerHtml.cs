﻿using System;
using System.Linq;
using System.Text;

namespace TyreDistributor
{
    public class ReceiptHandlerHtml : BaseReceiptHandler
    {
        public override string GetReceipt(Order order)
        {
            var totalAmount = 0d;
            var result = new StringBuilder($"<html><body><h1>Order Receipt for {order.Company}</h1>");
            if (order.Lines.Any())
            {
                result.Append("<ul>");
                foreach (var line in order.Lines)
                {
                    var thisAmount = CalculateAmounts(line);
                    result.Append($"<li>{line.Quantity} x {line.Tyre.Brand} {line.Tyre.Model} = {thisAmount.ToString("C")}</li>");
                    totalAmount += thisAmount;
                }
                result.Append("</ul>");
            }
            result.Append($"<h3>Sub-Total: {totalAmount.ToString("C")}</h3>");
            var tax = totalAmount * Order.TaxRate;
            result.Append($"<h3>Tax: {tax.ToString("C")}</h3>");
            result.Append($"<h2>Total: {(totalAmount + tax).ToString("C")}</h2>");
            result.Append("</body></html>");
            return result.ToString();
        }
    }
}