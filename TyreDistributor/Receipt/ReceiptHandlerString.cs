﻿using System;
using System.Text;

namespace TyreDistributor
{
    public class ReceiptHandlerString : BaseReceiptHandler
    {
        public override string GetReceipt(Order order)
        {
            var totalAmount = 0d;
            var result = new StringBuilder($"Order Receipt for {order.Company}{Environment.NewLine}");
            
            foreach (var line in order.Lines)
            {
                var thisAmount = CalculateAmounts(line);
                result.AppendLine($"\t{line.Quantity} x {line.Tyre.Brand} {line.Tyre.Model} = {thisAmount.ToString("C")}");
                totalAmount += thisAmount;
            }
            result.AppendLine($"Sub-Total: {totalAmount.ToString("C")}");
            var tax = totalAmount * Order.TaxRate;
            result.AppendLine($"Tax: {tax.ToString("C")}");
            result.Append($"Total: {(totalAmount + tax).ToString("C")}");
            return result.ToString();;
        }

      
    }
}