﻿namespace TyreDistributor
{

    // we could strong type this tyre classes instead of using const strings
    // e.g. SuvTyre, MiniTyre, EstateTyre typed
    // or use generics to describe the tyre Tyre<Suv>, Type<Mini> etc.. to make it more extensable.
    
    public class Tyre
    {
        public const string Suv = "Sports Utility Vehicle";
        public const string Mini = "Mini";
        public const string Estate = "Estate";
    
        public Tyre(string brand, string model, int price)
        {
            Brand = brand;
            Model = model;
            Price = price;
        }

        public string Brand { get; private set; }
        public string Model { get; private set; }
        public int Price { get; set; }
    }
}
