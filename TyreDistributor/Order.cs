﻿using System.Collections.Generic;

namespace TyreDistributor
{
    public class Order
    {
        // this could possibly be pulled out as a const and arguably shouldn't be tied directly to an order.
        public const double TaxRate = .0725d;

        public Order(string company)
        {
            Company = company;
        }

        public IList<Line> Lines { get; } = new List<Line>();

        public string Company { get; }

        public void AddLine(Line line)
        {
            Lines.Add(line);
        }
    }
}