﻿using System.ComponentModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TyreDistributor.Tests
{
    [TestClass]
    public class OrderTest
    {
        private static readonly Tyre AlfaRomeo = new Tyre("Alfa Romeo", Tyre.Suv, 100);
        private static readonly Tyre BmwMini = new Tyre("BMW", Tyre.Mini, 200);
        private static readonly Tyre BmwEstate = new Tyre("BMW", Tyre.Estate, 500);


        // would be better to use DI however as this is a self contained and small test case, but this would allow us to moq the outputs.

        private IReceiptHandler _receiptHandler;
        private IReceiptHandler _receiptHandlerHtml;

        public OrderTest()
        {
            _receiptHandler = new ReceiptHandlerString();
            _receiptHandlerHtml = new ReceiptHandlerHtml();
        }

        // another suggestion would be to use data test similar to nunit testcase that way you can centralise the text and make it easier to follow, rather than const all through the code.

        [DataTestMethod]
        [DataRow("Alfa Romeo", Tyre.Suv, 100, @"Order Receipt for Anywhere Tyre Shop
	1 x Alfa Romeo Sports Utility Vehicle = £100.00
Sub-Total: £100.00
Tax: £7.25
Total: £107.25", DisplayName = "String receipt - Alfa Romeo Tyre")]

        [DataRow("BMW", Tyre.Mini, 200, @"Order Receipt for Anywhere Tyre Shop
	1 x BMW Mini = £200.00
Sub-Total: £200.00
Tax: £14.50
Total: £214.50", DisplayName = "String receipt - Bmw Mini Tyre")]

        public void StringReceipt(string brand, string model, int price, string expectedOutput)
        {
            var order = new Order("Anywhere Tyre Shop");
            order.AddLine(new Line(new Tyre(brand, model, price), 1));
            Assert.AreEqual(expectedOutput, _receiptHandler.GetReceipt(order));
        } 

        
       
        [TestMethod]
        public void ReceiptOneAlfaRomeo()
        {
            var order = new Order("Anywhere Tyre Shop");
            order.AddLine(new Line(AlfaRomeo, 1));
            Assert.AreEqual(ResultStatementOneAlfa, _receiptHandler.GetReceipt(order));
        }

        private const string ResultStatementOneAlfa = @"Order Receipt for Anywhere Tyre Shop
	1 x Alfa Romeo Sports Utility Vehicle = £100.00
Sub-Total: £100.00
Tax: £7.25
Total: £107.25";


        /// <summary>
        /// Test suv for greater than 20 missing from coverage
        /// </summary>
        [TestMethod]
        public void ReceiptOneAlfaRomeoQuantityGreaterThan19()
        {
            var order = new Order("Anywhere Tyre Shop");
            order.AddLine(new Line(AlfaRomeo, 25));
            Assert.AreEqual(ResultStatementOneAlfaDiscounted, _receiptHandler.GetReceipt(order));
        }


        private const string ResultStatementOneAlfaDiscounted = @"Order Receipt for Anywhere Tyre Shop
	25 x Alfa Romeo Sports Utility Vehicle = £2,250.00
Sub-Total: £2,250.00
Tax: £163.13
Total: £2,413.13";


        [TestMethod]
        public void ReceiptOneBmw1()
        {
            var order = new Order("Anywhere Tyre Shop");
            order.AddLine(new Line(BmwMini, 1));
            Assert.AreEqual(ResultStatementOneBmw, _receiptHandler.GetReceipt(order));
        }

        private const string ResultStatementOneBmw = @"Order Receipt for Anywhere Tyre Shop
	1 x BMW Mini = £200.00
Sub-Total: £200.00
Tax: £14.50
Total: £214.50";


        
        /// Again added test 
        
        [TestMethod]
        public void ReceiptOneBmwQuantityGreaterThan9()
        {
            var order = new Order("Anywhere Tyre Shop");
            order.AddLine(new Line(BmwMini, 15));
            Assert.AreEqual(ResultStatementOneBmwDiscounted, _receiptHandler.GetReceipt(order));
        }

        private const string ResultStatementOneBmwDiscounted = @"Order Receipt for Anywhere Tyre Shop
	15 x BMW Mini = £2,400.00
Sub-Total: £2,400.00
Tax: £174.00
Total: £2,574.00";

        [TestMethod]
        public void ReceiptOneBmwX()
        {
            var order = new Order("Anywhere Tyre Shop");
            order.AddLine(new Line(BmwEstate, 1));
            Assert.AreEqual(ResultStatementOneBmwEstate, _receiptHandler.GetReceipt(order));
        }

        private const string ResultStatementOneBmwEstate = @"Order Receipt for Anywhere Tyre Shop
	1 x BMW Estate = £500.00
Sub-Total: £500.00
Tax: £36.25
Total: £536.25";


        [TestMethod]
        public void ReceiptOneBmwXQuantityGreaterThan4()
        {
            var order = new Order("Anywhere Tyre Shop");
            order.AddLine(new Line(BmwEstate, 5));
            Assert.AreEqual(ResultStatementOneBmwEstateDiscounted, _receiptHandler.GetReceipt(order));
        }

        private const string ResultStatementOneBmwEstateDiscounted = @"Order Receipt for Anywhere Tyre Shop
	5 x BMW Estate = £2,000.00
Sub-Total: £2,000.00
Tax: £145.00
Total: £2,145.00";

        [TestMethod]
        public void HtmlReceiptOneAlfaRomeoSportWagon()
        {
            var order = new Order("Anywhere Tyre Shop");
            order.AddLine(new Line(AlfaRomeo, 1));
            Assert.AreEqual(HtmlResultStatementOneAlfaRomeoSportWagon, _receiptHandlerHtml.GetReceipt(order));
        }

        private const string HtmlResultStatementOneAlfaRomeoSportWagon = @"<html><body><h1>Order Receipt for Anywhere Tyre Shop</h1><ul><li>1 x Alfa Romeo Sports Utility Vehicle = £100.00</li></ul><h3>Sub-Total: £100.00</h3><h3>Tax: £7.25</h3><h2>Total: £107.25</h2></body></html>";

        [TestMethod]
        public void HtmlReceiptOneBmwMiniSeries()
        {
            var order = new Order("Anywhere Tyre Shop");
            order.AddLine(new Line(BmwMini, 1));
            Assert.AreEqual(HtmlResultStatementOneBmw1Series, _receiptHandlerHtml.GetReceipt(order));
        }

        private const string HtmlResultStatementOneBmw1Series = @"<html><body><h1>Order Receipt for Anywhere Tyre Shop</h1><ul><li>1 x BMW Mini = £200.00</li></ul><h3>Sub-Total: £200.00</h3><h3>Tax: £14.50</h3><h2>Total: £214.50</h2></body></html>";

        [TestMethod]
        public void HtmlReceiptOneBmwEstate()
        {
            var order = new Order("Anywhere Tyre Shop");
            order.AddLine(new Line(BmwEstate, 1));
            Assert.AreEqual(HtmlResultStatementOneBmwX5, _receiptHandlerHtml.GetReceipt(order));
        }

        private const string HtmlResultStatementOneBmwX5 = @"<html><body><h1>Order Receipt for Anywhere Tyre Shop</h1><ul><li>1 x BMW Estate = £500.00</li></ul><h3>Sub-Total: £500.00</h3><h3>Tax: £36.25</h3><h2>Total: £536.25</h2></body></html>";    
    }
}


